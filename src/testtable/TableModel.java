/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testtable;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author 60160019
 */
public class TableModel extends AbstractTableModel {

    String[] columnsName = {"ID","Firstname"};
    ArrayList<user> UserList = data.userList;
    public TableModel() {
        
    }

    @Override
    public String getColumnName(int column) {
        return columnsName[column];
    }

    @Override
    public int getRowCount() {
        return UserList.size();
    }

    @Override
    public int getColumnCount() {
        return columnsName.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        user u = UserList.get(rowIndex);
        if(u == null) return "";
        switch(columnIndex){
            case 0 : return u.getID();
            case 1 : return u.getUsername();
        }
        return "";
    }

}
