/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testtable;

import java.util.ArrayList;

/**
 *
 * @author 60160019
 */
class user {

    private String ID, Username, Fristname, Lastname;
    static ArrayList<user> UserList;
    public user(){
    }

    public user(String ID, String Username, String Fristname, String Lastname) {
       this.ID = ID;
       this.Username = Username;
       this.Fristname = Fristname;
       this.Lastname = Lastname;
    }
    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public String getFristname() {
        return Fristname;
    }

    public void setFristname(String Fristname) {
        this.Fristname = Fristname;
    }

    public String getLastname() {
        return Lastname;
    }

    public void setLastname(String Lastname) {
        this.Lastname = Lastname;
    }

    public static ArrayList<user> getUserList() {
        return UserList;
    }
}
